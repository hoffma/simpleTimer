Here you can find a multiplatform timer library.\
It has been tested with GCC on Linux and MinGW on Windows.\
It is licensed under the [GPL version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt "GPL version 2").

Originally taken from here: https://www.teuniz.net/Timer_code/index.html


I don't know how accurate it is and I don't care. It's a cute timer that just works (tm) for smol projects
